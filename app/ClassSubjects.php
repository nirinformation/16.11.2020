<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassSubjects extends Model
{
    protected $table = 'class_subjects';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['sections_id','grades_id','all_classes_id','subject_no','subject_name'];
}
