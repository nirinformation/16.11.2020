<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseStudents extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'course_students';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function students(){
        return $this->belongsTo(Students::class,'student_id','id');
    }

    public function courses(){
        return $this->belongsTo(Courses::class,'course_id','id');
    }

    public function batch(){
        return $this->belongsTo(Batch::class,'batch_id','id');
    }
}
