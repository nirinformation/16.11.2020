<?php

namespace App\Http\Controllers;

use App\Exam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Mail\StudentRegisterMail;
use Illuminate\Support\Facades\Mail;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('exam.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            DB::beginTransaction();
            $rules = [
                'add_exam_name'        =>  'required|unique:tbl_exam,exam_name',
                'add_exam_category'    =>  'required',
                'add_exam_aca_year'    =>  'required',
                'add_exam_term'        =>  'required',
                'add_exam_start_date'  =>  'required',
                'add_exam_end_date'    =>  'required',
                'add_exam_status'      =>  'required',
            ];
            $customMessages = [
                'add_exam_name.required'       =>  'exam name is required',
                'add_exam_name.unique'         =>  'exam name is already exist',
                'add_exam_category.required'   =>  'exam category is required',
                'add_exam_aca_year.required'   =>  'exam academic year is required',
                'add_exam_term.required'       =>  'exam term is required',
                'add_exam_start_date.required' =>  'exam start date is required',
                'add_exam_end_date.required'   =>  'exam end date is required',
                'add_exam_status.required'     =>  'exam status is required',
                
            ];
            $validator =Validator::make($request->all(), $rules, $customMessages);
            $data = [
                'exam_name'         => $request['add_exam_name'],
                'exam_category'     => $request['add_exam_category'],
                'exam_aca_year'     => $request['add_exam_aca_year'],
                'exam_term'         => $request['add_exam_term'],
                'exam_start_date'   => $request['add_exam_start_date'],
                'exam_end_date'     => $request['add_exam_end_date'],
                'exam_status'       => $request['add_exam_status'],
                'created_by'        => Auth::id(),
                'updated_by'        => Auth::id(),
            ];
            $result = Exam::create($data);


            return response()->json([
                'msg' => 'Record is successfully added',
                'success' => true,
                'data' => [],
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        try {
            DB::beginTransaction();
            $rules = [
                'edit_exam_name'        =>  'required|unique:tbl_exam,exam_name',
                'edit_exam_category'    =>  'required',
                'edit_exam_aca_year'    =>  'required',
                'edit_exam_term'        =>  'required',
                'edit_exam_start_date'  =>  'required',
                'edit_exam_end_date'    =>  'required',
                'edit_exam_status'      =>  'required',
            ];
            $customMessages = [
                'edit_exam_name.required'       =>  'exam name is required',
                'edit_exam_name.unique'         =>  'exam name is already exist',
                'edit_exam_category.required'   =>  'exam category is required',
                'edit_exam_aca_year.required'   =>  'exam academic year is required',
                'edit_exam_term.required'       =>  'exam term is required',
                'edit_exam_start_date.required' =>  'exam start date is required',
                'edit_exam_end_date.required'   =>  'exam end date is required',
                'edit_exam_status.required'     =>  'exam status is required',
                
            ];
            $validator =Validator::make($request->all(), $rules, $customMessages);
            $data = [
                'exam_name'         => $request['edit_exam_name'],
                'exam_category'     => $request['edit_exam_category'],
                'exam_aca_year'     => $request['edit_exam_aca_year'],
                'exam_term'         => $request['edit_exam_term'],
                'exam_start_date'   => $request['edit_exam_start_date'],
                'exam_end_date'     => $request['edit_exam_end_date'],
                'exam_status'       => $request['edit_exam_status'],
                'created_by'        => Auth::id(),
                'updated_by'        => Auth::id(),
            ];

            Exam::where('exam_id', $id)->update($data);
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Section has been updated',
            ];
            return response()->json($response);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            Exam::where('exam_id', $id)->delete();
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Data has been Deleted',
                'httpStatus' => 201
            ];
            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'data' => [],
                'msg' => 'Data has not been Deleted',
                'httpStatus' => 500
            ];
            return response()->json($response);
        }
    }

    /**
     * Get All Students
     *
     * @return void
     */
    public function getExamData()
    {
        $exam = Exam::latest()->get();

        return response()->json(['data' => $exam]);
    }
}