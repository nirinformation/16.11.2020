<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Mail\StudentRegisterMail;
use Illuminate\Support\Facades\Mail;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('students.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            DB::beginTransaction();
            $validator = validator::make($request->all(), [
                'firstname' => 'required',
                'lastname'  => 'required',
                'mobile'    =>  'required',

            ]);
            if ($request->addimage) {
                $image = $request->file('addimage');

                $new_name = rand() . '.' . $image->getClientOriginalExtension();

                $image->move(public_path('images'), $new_name);
            }


            $array = [
                'fname' => $request['firstname'],
                'lname' => $request['lastname'],
                'mobile' => $request['mobile'],
                'telephone' => $request['telephone'],
                'email' => $request['email'],
                'dob' => $request['dob'],
                'nic' => $request['nic'],
                'image' => $new_name,
                'created_by' => Auth::id(),
                'updated_by' => Auth::id(),
            ];
            $result = Student::create($array);


            return response()->json([
                'msg' => 'Record is successfully added',
                'success' => true,
                'data' => [],
            ]);
            $this->sendRegisterEmail($result);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }
    public function sendRegisterEmail($data)
    {

        return Mail::to('nirdetails@gmail.com')
            ->cc($data['email'])
            ->send(new StudentRegisterMail($data));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // array:9 [
        //     "_token" => "wyDYqXnrQ5haIS33Z6PJMIYoW3ZWpddowcNGcYvt"
        //     "firstname" => "Gayan"
        //     "lastname" => "Perera"
        //     "mobile" => "0770125125"
        //     "hdnid" => "1"
        //     "telephone" => "0112525525"
        //     "email" => "gayan@gmail.com"
        //     "dob" => "1990-10-20"
        //     "nic" => "902521251v"
        //   ]
        try {
            $array = [
                'fname' => $request['firstname'],
                'lname' => $request['lastname'],
                'mobile' => $request['mobile'],
                'telephone' => $request['telephone'],
                'email' => $request['email'],
                'dob' => $request['dob'],
                'nic' => $request['nic'],
            ];

            Student::where('id', $id)->update($array);
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Student has been updated',
            ];
            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'data' => [],
                'msg' => 'Student has not been updated',
            ];
            return response()->json($response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            Student::where('id', $id)->delete();
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Data has been Deleted',
                'httpStatus' => 201
            ];
            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'data' => [],
                'msg' => 'Data has not been Deleted',
                'httpStatus' => 500
            ];
            return response()->json($response);
        }
    }

    /**
     * Get All Students
     *
     * @return void
     */
    public function getStudentData()
    {
        $students = Student::where('status', 1)->get();

        return response()->json(['data' => $students]);
    }
}