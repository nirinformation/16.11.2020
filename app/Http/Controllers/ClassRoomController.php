<?php

namespace App\Http\Controllers;

use App\ClassRoom;
use App\Grade;
use App\Section;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request as Request;
use Illuminate\Support\Facades\Request as Input;
use Illuminate\Support\Facades\Validator;
use App\Mail\StudentRegisterMail;
use Illuminate\Support\Facades\Mail;


class ClassRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $section  = DB::table('tbl_section')->orwhereNull('deleted_at')->get();

        $grade = DB::table('tbl_section')
            ->join('tbl_grade', 'tbl_grade.gra_sec_id', '=', 'tbl_section.sec_id')
            ->orwhereNull('tbl_grade.deleted_at')
            ->get();
        return view('classroom.index', compact('section', 'grade'));
    }

    public function gradedynamic()
    {
        $sect = Input::get('sect_id');
        $grades = Grade::where('gra_sec_id', '=', $sect)->get();

        return response()->json($grades);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            DB::beginTransaction();
            $rules = [

                'add_clas_room_sec_id'    =>  'required',
                'add_clas_room_gra_id'    =>  'required',
                'add_clas_room_name'      =>  'required|unique:tbl_class_room,clas_room_name',
            ];
            $customMessages = [

                'add_clas_room_sec_id.required'  =>  'Section name is required',
                'add_clas_room_gra_id.required'  =>  'Grade name is required',
                'add_clas_room_name.required'    =>  'Class room is required',
                'add_clas_room_name.unique'      =>  'Class room name is already exist',

            ];
            $validator = Validator::make($request->all(), $rules, $customMessages);

            $data = [
                'clas_room_sec_id' => $request['add_clas_room_sec_id'],
                'clas_room_gra_id' => $request['add_clas_room_gra_id'],
                'clas_room_name' => $request['add_clas_room_name'],
                'created_by' => Auth::id(),
                'updated_by' => Auth::id(),
            ];
            $result = ClassRoom::insert($data);


            return response()->json([
                'msg' => 'Record is successfully added',
                'success' => true,
                'data' => [],
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            DB::beginTransaction();

            $rules = [

                'edit_clas_room_name'      =>  'required|unique:tbl_class_room,clas_room_name',
            ];
            $customMessages = [

                'edit_clas_room_name.required'  =>  'Class room name is required',
                'edit_clas_room_name.unique'      =>  'Class room name is already exist',

            ];
            $validator = validator::make($request->all(), $rules, $customMessages);


            $data = [
                'clas_room_name' => $request['edit_clas_room_name'],

            ];

            ClassRoom::where('clas_room_id', $id)->update($data);
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Section has been updated',
            ];
            return response()->json($response);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            DB::beginTransaction();
            $data = ClassRoom::findOrFail($id);
            $data->delete();
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Data has been Deleted',
                'httpStatus' => 201
            ];
            return response()->json($response);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'data' => [],
                'msg' => 'Data has not been Deleted',
                'httpStatus' => 500
            ];
            return response()->json($response);
        }
    }

    /**
     * Get All Students
     *
     * @return void
     */
    public function getClassRoomData()
    {

        $classroom = DB::table('tbl_section')
            ->join('tbl_grade', 'tbl_grade.gra_sec_id', '=', 'tbl_section.sec_id')
            ->join('tbl_class_room', 'tbl_class_room.clas_room_gra_id', '=', 'tbl_grade.gra_id')
            ->orwhereNull('tbl_class_room.deleted_at')
            ->get();

        return response()->json(['data' => $classroom]);
    }
}