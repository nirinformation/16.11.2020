<?php

namespace App\Http\Controllers;

use App\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Mail\StudentRegisterMail;
use Illuminate\Support\Facades\Mail;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('section.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            DB::beginTransaction();
            $rules = [
                'add_sec_name'    =>  'required|unique:tbl_section,sec_name',
            ];
            $customMessages = [
                'add_sec_name.required' =>  'Section name is required',
                'add_sec_name.unique'   =>  'Section name is already exist'

            ];
            $validator =Validator::make($request->all(), $rules, $customMessages);

            $data = [
                'sec_name' => $request['add_sec_name'],

            ];
            $data = [
                'sec_name' => $request['add_sec_name'],
                'created_by' => Auth::id(),
                'updated_by' => Auth::id(),
            ];
            $result = Section::create($data);


            return response()->json([
                'msg' => 'Record is successfully added',
                'success' => true,
                'data' => [],
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            DB::beginTransaction();
            $rules = [
                'edit_sec_name'    =>  'required|unique:tbl_section,sec_name',
            ];
            $customMessages = [
                'edit_sec_name.required' =>  'Section name is required',
                'edit_sec_name.unique'   =>  'Section name is already exist'

            ];
            $validator = validator::make($request->all(), $rules, $customMessages);

            $data = [
                'sec_name' => $request['edit_sec_name'],

            ];

            Section::where('sec_id', $id)->update($data);
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Section has been updated',
            ];
            return response()->json($response);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            Section::where('sec_id', $id)->delete();
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Data has been Deleted',
                'httpStatus' => 201
            ];
            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'data' => [],
                'msg' => 'Data has not been Deleted',
                'httpStatus' => 500
            ];
            return response()->json($response);
        }
    }

    /**
     * Get All Students
     *
     * @return void
     */
    public function getSectionData()
    {
        $section = Section::latest()->get();

        return response()->json(['data' => $section]);
    }
}