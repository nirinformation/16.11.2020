<?php

namespace App\Http\Controllers;

use App\Lecturer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LecturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //All Lecturer get
        try{
            $data = Lecturer::all();
            $response = [
                'success'=>true,
                'data'=>$data,
                'msg'=>'Data has been found',
                'httpStatus'=> 200
            ];
        }catch(\Exception $e){
            $response = [
                'success'=>false,
                'data'=>[],
                'msg'=>'Data has not been found',
                'httpStatus'=> 500
            ];
        }
        return response()->json($response,$response['httpStatus']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        try{


            DB::beginTransaction();
            $array = [
                'name'=>$request['name'],
                'email'=>$request['email'],
                'mobile'=>$request['mobile'],
                'qualification'=>$request['qualification'],
                
            ];

            $result = Lecturer::create($array);
            $response = [
                'success'=>true,
                'data'=>$result,
                'msg'=>'Lecturer Has been created',
                'httpStatus'=> 201
            ];
            DB::commit();
        }catch(\Exception $e){
            DB::rollBack();
            $response = [
                'success'=>false,
                'data'=>$result,
                'msg'=>'Lecturer has not been created',
                'httpStatus'=> 500
            ];
        }

        return response()->json($response,$response['httpStatus']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       try{
        $result = Lecturer::find($id);

        $response = [
            'success'=>true,
            'data'=>$result,
            'msg'=>'Data has been found',
            'httpStatus'=> 200
        ];

       }catch(\Exception $e){
        $response = [
                    'success'=>false,
                    'data'=>[],
                    'msg'=>'Data has not been found',
                    'httpStatus'=> 500
            ];
       }

       return response()->json($response,$response['httpStatus']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      
        try{
            $result = Lecturer::find($id);
                 
            $array = [
                'name'=>$request['name'],
                'email'=>$request['email'],
                'mobile'=>$request['mobile'],
                'qualification'=>$request['qualification'],
            ];
            $result=Lecturer::where('id',$id)->update($array); 
            $result = Lecturer::find($id);
            $response = [
                'success'=>true,
                'data'=>$result,
                'msg'=>'Lecturer Has been created',
                'httpStatus'=> 201
            ];
           
        }catch(\Exception $e){
           
            $response = [
                'success'=>false,
                'data'=>$result,
                'msg'=>'Lecturer has not been created',
                'httpStatus'=> 500
            ];
        }

        return response()->json($response,$response['httpStatus']);
                 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Lecturer::where('id', $id)->delete();
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Data has been Deleted',
                'httpStatus'=> 201
            ];
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'data' => [],
                'msg' => 'Data has not been Deleted',
                'httpStatus'=> 500
            ];
        }

        return response()->json($response);
    }
    
}