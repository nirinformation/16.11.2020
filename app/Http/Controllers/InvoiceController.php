<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Invoice;
use App\InvoiceDetails;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('invoice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // array:5 [
        //     "_token" => "eCzpIUTHeNqcGzm3xvsxjJJtW6ESxZKYUfMWgaPc"
        //     "customer_name" => "yy"
        //     "date" => "2019-11-18"
        //     "row_no" => "3"
        //     "items" => array:3 [
        //       0 => array:3 [
        //         "qty" => "3"
        //         "unit_price" => "3"
        //         "sub_total" => null
        //       ]
        //       1 => array:3 [
        //         "qty" => "4"
        //         "unit_price" => "4"
        //         "sub_total" => null
        //       ]
        //       2 => array:3 [
        //         "qty" => "5"
        //         "unit_price" => "5"
        //         "sub_total" => null
        //       ]
        //     ]
        //   ]

        try {

            DB::beginTransaction();
            $invoiceArray = [
                'customer_name' => $request['customer_name'],
                'date' => $request['date'],
                'total_amount' => $request['total'],
            ];

            $invoice = Invoice::create($invoiceArray);
            $invoiceId = $invoice->id;
            $invoiceItemArray = [];
            foreach ($request['items'] as $item) {
                $itemArray = [
                    'invoice_id' => $invoiceId,
                    'item_name' => $item['item_name'],
                    'qty' => $item['qty'],
                    'unit_price' => $item['unit_price'],
                    'sub_total' => $item['sub_total'],
                    'created_at' => date('y-m-d H:i:s'),
                    'updated_at' => date('y-m-d H:i:s'),
                ];
                array_push($invoiceItemArray, $itemArray);
            }
            $invoice->InvoiceDetails()->insert($invoiceItemArray);


            DB::commit();
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Invoice has been created',
                'httpStatus' => 200
            ];
            return  response()->json($response);
        } catch (\Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'data' => [],
                'msg' => 'Invoice has not been created',
                'httpStatus' => 500
            ];
            return  response()->json($response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}