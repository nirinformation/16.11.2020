<?php

namespace App\Http\Controllers;

use App\Parents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Mail\StudentRegisterMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
class ParentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        return view('parents.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $parents_data=Parents::where('parents_stu_id',$request->add_parents_stu_id)
        ->where('parents_type',$request->add_parents_type)
        ->count();
        if($parents_data>0){
         return response()->json([
                'msg' => 'Parents already add',
                'success' => true,
               
            ]);
        }else{
        try {
            DB::beginTransaction();
           
           
            $rules = [
               
                'add_parents_stu_id'                 =>  'required',
                'add_parents_type'                   =>  'required',
                'add_parents_name'                   =>  'required',
                'add_parents_occu'                   =>  'required',
                'add_parents_nic'                    =>  'required',
                'add_parents_mobile'                 =>  'required', 
                'add_parents_email'                  =>  'required',
                'add_parents_name_of_employ'         =>  'required',
                'add_parents_addre_of_employ'        =>  'required',
                'add_parents_office_tel'             =>  'required',
            ];
            $customMessages = [
               'add_parents_stu_id.required'          =>  'dfdf',
               'add_parents_type.unique'              =>  'Parents Type is already exist',
                'add_parents_type.required'            =>  'Parents Type is required',
                'add_parents_name.required'            =>  'Name is required',
                'add_parents_occu.required'            =>  'Occupation year is required',
                'add_parents_nic.required'             =>  'NIC is required',
                'add_parents_mobile.required'          =>  'Mobile No.  is required',
                'add_parents_email.required'           =>  'Email Address is required',
                'add_parents_name_of_employ.required'  =>  'Name of the Employement is required',
                'add_parents_addre_of_employ.required' =>  'Address of the Employement is required',
                'add_parents_office_tel.required'      =>  'Office Telephone No. is required',
               ];
            $validator =Validator::make($request->all(), $rules, $customMessages);
            $data = [
                'parents_stu_id'                   => $request['add_parents_stu_id'],
                'parents_type'                     => $request['add_parents_type'],
                'parents_name'                     => $request['add_parents_name'],
                'parents_occu'                     => $request['add_parents_occu'],
                'parents_nic'                      => $request['add_parents_nic'],
                'parents_mobile'                   => $request['add_parents_mobile'],
                'parents_email'                    => $request['add_parents_email'],
                'parents_name_of_employ'           => $request['add_parents_name_of_employ'],
                'parents_addre_of_employ'          => $request['add_parents_addre_of_employ'],
                'parents_office_tel'               => $request['add_parents_office_tel'],
                'created_by'                       => Auth::id(),
                'updated_by'                       => Auth::id(),
            ];
            $result = Parents::insert($data);


            return response()->json([
                'msg' => 'Record is successfully added',
                'success' => true,
                'data' => [],
            ]);
            
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'msg'=>'dfdfdf',
                'data' => [],
            ]);
        }
    }
 
}
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $std_id = $id;
        return view('parents/index',compact('std_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // try {
        //     DB::beginTransaction();
        //     $rules = [
        //         //'edit_parents_stu_id'               =>  'required|unique:tbl_parents,parents_stu_id',
        //         'edit_parents_type'                   =>  'required',
        //         'edit_parents_name'                   =>  'required',
        //         'edit_parents_occu'                   =>  'required',
        //         'edit_parents_nic'                    =>  'required',
        //         'edit_parents_mobile'                 =>  'required',
        //         'edit_parents_email'                  =>  'required',
        //         'edit_parents_name_of_employ'         =>  'required',
        //         'edit_parents_addre_of_employ'        =>  'required',
        //         'edit_parents_office_tel'             =>  'required',
        //     ];
        //     $customMessages = [
        //        // 'edit_parents_stu_id.required'        =>  'student id is required',
        //         'edit_parents_type.unique'              =>  'required|unique:tbl_parents,parents_type',
        //         'edit_parents_type.required'            =>  'Parents Type is required',
        //         'edit_parents_name.required'            =>  'Name is required',
        //         'edit_parents_occu.required'            =>  'Occupation year is required',
        //         'edit_parents_nic.required'             =>  'NIC is required',
        //         'edit_parents_mobile.required'          =>  'Mobile No.  is required',
        //         'edit_parents_email.required'           =>  'Email Address is required',
        //         'edit_parents_name_of_employ.required'  =>  'Name of the Employement is required',
        //         'edit_parents_addre_of_employ.required' =>  'Address of the Employement is required',
        //         'edit_parents_office_tel.required'      =>  'Office Telephone No. is required',
        //        ];
        //     $validator =Validator::make($request->all(), $rules, $customMessages);
        //     $data = [
        //         //'parents_stu_id'                 => $request['edit_parents_stu_id'],
        //         'parents_type'                     => $request['edit_parents_type'],
        //         'parents_name'                     => $request['edit_parents_name'],
        //         'parents_occu'                     => $request['edit_parents_occu'],
        //         'parents_nic'                      => $request['edit_parents_nic'],
        //         'parents_mobile'                   => $request['edit_parents_mobile'],
        //         'parents_email'                    => $request['edit_parents_email'],
        //         'parents_name_of_employ'           => $request['edit_parents_name_of_employ'],
        //         'parents_addre_of_employ'          => $request['edit_parents_addre_of_employ'],
        //         'parents_office_tel'               => $request['edit_parents_office_tel'],
        //         'created_by'                       => Auth::id(),
        //         'updated_by'                       => Auth::id(),
        //     ];

        //     Parents::where('parents_id', $id)->update($data);
        //     $response = [
        //         'success' => true,
        //         'data' => [],
        //         'msg' => 'Parents has been updated',
        //     ];
        //     return response()->json($response);
        //     DB::commit();
        // } catch (\Exception $e) {
        //     DB::rollBack();
        //     return response()->json([
        //         'errors' => $validator->errors()->all(),
        //         'success' => false,
        //         'data' => [],

        //     ]);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            Parents::where('parents_id', $id)->delete();
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Data has been Deleted',
                'httpStatus' => 201
            ];
            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'data' => [],
                'msg' => 'Data has not been Deleted',
                'httpStatus' => 500
            ];
            return response()->json($response);
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getParentsData(Request $request)
    {

     $parentd = DB::table('tbl_student')
    ->join('tbl_parents', 'tbl_parents.parents_stu_id', '=', 'tbl_student.stu_id')
    ->select('tbl_parents.*')
    ->where('tbl_parents.parents_stu_id', '=', $request->id)
    ->whereNull('tbl_student.deleted_at')
    ->get();

        return response()->json(['data' => $parentd]);
    }



}