<?php

namespace App\Http\Controllers;

ini_set('max_execution_time', 1200);

use App\ClassRoom;
use App\Grade;
use App\Section;
use App\Student;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request as Request;
use Illuminate\Support\Facades\Request as Input;
use Illuminate\Support\Facades\Validator;
use App\Mail\StudentRegisterMail;
use Illuminate\Support\Facades\Mail;

use PDF;

class PdfConverterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }
    public function mypdf()
    {

        $section  = DB::table('tbl_section')->orwhereNull('deleted_at')->get();
        $grade  =   DB::table('tbl_grade')
            ->join('tbl_section', 'tbl_section.sec_id', '=', 'tbl_grade.gra_sec_id')
            ->orwhereNull('tbl_grade.deleted_at')
            ->get();
        $classroom = DB::table('tbl_class_room')
            ->join('tbl_grade', 'tbl_grade.gra_id', '=', 'tbl_class_room.clas_room_gra_id')
            ->orwhereNull('tbl_class_room.deleted_at')
            ->get();

        $student = DB::table('tbl_section')
            ->join('tbl_grade', 'tbl_grade.gra_sec_id', '=', 'tbl_section.sec_id')
            ->join('tbl_class_room', 'tbl_class_room.clas_room_gra_id', '=', 'tbl_grade.gra_id')
            ->join('tbl_student', 'tbl_student.stu_clas_room_id', '=', 'tbl_class_room.clas_room_id')
            ->paginate(2);

        $pdf = PDF::loadView('studentc.abc', compact('section', 'grade', 'classroom', 'student'));

        return $pdf->download('myfile.pdf');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    /**
     * Get All Students
     *
     * @return void
     */
    public function getSectionData()
    {
    }
}