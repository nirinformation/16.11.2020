<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = "invoice";
    protected $primaryKey = "id";
    protected $guarded = [];
    public function InvoiceDetails()
    {
        return $this->hasMany(InvoiceDetails::class, 'invoice_id', 'id');
    }
}