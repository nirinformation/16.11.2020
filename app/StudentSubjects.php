<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentSubjects extends Model
{
   protected $table = 'student_subjects';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['all_classes_id','student_id','date','term','subject_id','marks','marks_grade'];
}
