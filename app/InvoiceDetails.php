<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetails extends Model
{
    protected $table = "invoice_details";
    protected $primaryKey = "id";
    protected $guarded = [];
    public function invoice()
    {
        return $this->belongsTo(invoice::class, 'invoice_id', 'id');
    }
}