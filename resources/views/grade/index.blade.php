@extends('dashboard.index')
@section('content')
    <div class="row">

        <div class="col-md-12 text-right">
           <button class="btn btn-primary" onclick="loadAddModal()">Add Grade</button>
        </div>
        <div class="col-md-12 mt-3">
            <table class="table table-bordered table-hover" id="tblgrade">
                <thead>
                    <tr>
                        <th>Section</th>
                        <th>Grade Name</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th>Section</th>
                        <th>Grade Name</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@include('grade.components.edit-grade-modal')
@include('grade.components.add-grade-modal')
@endsection

@section('custom-js')
<script type="text/javascript">
$.ajaxSetup({
    beforeSend: function(xhr, type) {
        if (!type.crossDomain) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        }
    },
});
   
    $(document).ready( function () {
    $('#tblgrade').DataTable({
    ajax: baseurl+'/get-grade-data',
    columns: [
        { data: 'sec_name' },
        { data: 'gra_name' },
        
        {
            data: null,
            className: "center",
            defaultContent: '<a href="" class="editor_edit btn btn-primary inner float:right"><i class="fas fa-edit"></i></a>' +
                    '<a href="" class="remove_user btn btn-danger inner"><i class="fas fa-trash-alt"></i></a>'
        }

    ]
});

});

// Edit record
$('#tblgrade').on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        var data = $('#tblgrade').DataTable().row($(this).parents('tr')).data();
        $('#edit_gra_name').val(data.gra_name);
        $('#hdnid').val(data.gra_id);
        $('#gradeeditmodal').modal('toggle');


});

$('#frmeditgrade').submit(function(e){
    e.preventDefault();
    var id = $('#hdnid').val();

    $.ajax({
        type:'PUT',
        url: baseurl+'/admin/grade/'+id,
        data: $('#frmeditgrade').serialize(),
        success: function(data){
            if(data.success==true){
                alert(data.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<p>'+value+'</p>');
                          });
            }
        }
    });

});
$('#frmaddgrade').submit(function(e){
    e.preventDefault();
    $.ajax({
        type:'POST',

        url: baseurl+'/admin/grade/',
        contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
        data: new FormData(this),
        success: function(data){

            if(data.success==true){
                alert(data.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
               
                jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<p>'+value+'</p>');
                          });
                                  
                        
            }
           
        }
    });

});
//Add rocord
function loadAddModal(){
    $('#gradeaddmodal').modal('toggle');
    
}

$('#tblgrade').on('click', 'a.remove_user', function (e) {
        e.preventDefault();
       
        var data = $('#tblgrade').DataTable().row($(this).parents('tr')).data();
        var id=data.gra_id;
        
        $.confirm({
    title: 'Confirm!',
    content: 'Simple confirm!',
    buttons: {
        confirm: function () {
            $.ajax({
        type:'delete',
        url: baseurl+'/admin/grade/'+id,
        
        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg);
            }
        }
    });
        },
        cancel: function () {
            
        },
        
    }
});








        /* $.ajax({
        type:'DELETE',
        url: baseurl+'/admin/grade/'+id,
        
        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg);
            }
        }
    }); */


});


</script>

@endsection
