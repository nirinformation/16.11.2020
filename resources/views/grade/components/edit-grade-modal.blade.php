<!-- Modal -->
<div class="modal fade" id="gradeeditmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Grade</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
   
      <div class="alert alert-danger" style="display:none"></div>
      <form id="frmeditgrade">
      <div class="modal-body">
              {{ csrf_field() }}
              <div class="form-group">
                <label class="col-form-label">Grade Name</label>
                <input type="text" class="form-control" id="edit_gra_name" name="edit_gra_name"/>
              </div>
              <input type="hidden" name="hdnid" id="hdnid"/>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
  </form>
    </div>
  </div>
</div>
