<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>My Project</title>
<link rel="stylesheet" href="{{asset('library/css/bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('library/css/datatables.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('library/css/jquery-confirm.css')}}"/>
@yield('custom-css')
</head>
<body>
    {{-- https://via.placeholder.com/150 --}}
    {{-- @if(Auth::user()->image_path=='')
    <img src="https://via.placeholder.com/150"/>
    @else--}}
    {{-- <img style="border-radius: 100%" src="{{asset(Auth::user()->image_path)}}"/> --}}
    <img class="ml-5 mt-5" style="border-radius: 100%; width:80px; height:80px" src="{{asset('library/img/logo.jpg')}}"/>
    {{-- @endif --}}
    
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Students
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                
              <a class="dropdown-item" href="{{url('admin/section')}}">Section</a>
              <a class="dropdown-item" href="{{url('admin/grade')}}">Grade</a>
              <a class="dropdown-item" href="{{url('admin/classroom')}}">Class Room</a>
              <a class="dropdown-item" href="{{url('admin/student')}}">student</a>
              <a class="dropdown-item" href="{{url('admin/teacher')}}">teacher</a>
              <a class="dropdown-item" href="{{url('admin/subject')}}">subject</a>
                {{-- <a class="dropdown-item" href="#">Add Student</a> --}}
                <div class="dropdown-divider"></div>
                {{-- <a class="dropdown-item" href="#">Something else here</a> --}}
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link disabled" href="#">Disabled</a>
            </li>
          </ul>
        
        <ul class="navbar-nav mr-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Profile
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{url('logout')}}">logout</a>
            </li>
        </ul>
        </div>
      </nav>
      <div class="container">
          @yield('content')
      </div>

    <script src="{{asset('library/js/jquery-3.5.1.js')}}"></script>
    <script src="{{asset('library/js/bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('library/js/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('library/js/jquery-confirm.js')}}"></script>
    @yield('custom-js')
    <script type="text/javascript">
     var baseurl ="{{url('')}}";
     
    </script>
</body>
</html>
