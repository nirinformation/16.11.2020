<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Project</title>
<link rel="stylesheet" href="{{asset('library/css/bootstrap.css')}}">
@yield('custom-css')
</head>
<body>

      <div class="container">
          @yield('content')
      </div>

    <script src="{{asset('library/js/jquery-3.5.1.js')}}"></script>
    <script src="{{asset('library/js/bootstrap.js')}}"></script>
    @yield('custom-js')
</body>
</html>
