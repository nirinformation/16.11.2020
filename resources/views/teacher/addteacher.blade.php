@extends('dashboard.index')
@section('content')
<!-- Modal -->
<div class="col-lg-12">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">New Teacher</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form id="frmaddteacher" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}

                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label for="add_tea_employee_no">Employee No</label>

                            <input type="text" name="add_tea_employee_no" id="add_employee_no" class="form-control" />

                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="add_tea_name_w_ini">Name with Intitials</label>
                            <input type="text" name="add_tea_name_w_ini" id="add_tea_name_w_ini" class="form-control" />
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="add_tea_full_name">Full Name</label>
                            <input type="text" name="add_tea_full_name" id="add_tea_full_name" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label name="add_tea_nic">NIC No</label>
                            <input type="text" name="add_tea_nic" id="add_tea_nic" class="form-control" />
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Date of Birth</label>

                            <input type="text" name="add_tea_dob" id="add_tea_dob" class="form-control datepicker" />
                        </div>

                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Gender </label>

                            <select name="add_tea_gender" id="add_tea_gender" class="form-control" />
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Civil Status </label>

                            <select name="add_tea_civil_status" id="add_tea_civil_status" class="form-control" />
                            <option value="Married">Married</option>
                            <option value="Single">Single</option>
                            <option value="Male">Divorced</option>
                            <option value="Female">Other</option>
                            </select>

                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Date of Service Start On</label>

                            <input type="text" name="add_tea_service_start_on" id="add_tea_service_start_on"
                                class="form-control datepicker" />

                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label class="control-label">Date of Service Start On (This School)</label>

                            <input type="text" name="add_tea_service_start_on_this_school"
                                id="add_tea_service_start_on_this_school" class="form-control datepicker" />

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Home Town</label>

                            <input type="text" name="add_tea_home_town" id="add_tea_home_town" class="form-control" />
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Type of Transport</label>

                            <input type="text" name="add_tea_type_of_trans" id="add_tea_type_of_trans"
                                class="form-control" />

                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Service Medium</label>

                            <select name="add_tea_service_medium" id="add_tea_service_mediumstu_per_addre"
                                class="form-control" />
                            <option value="Sinhala">Sinhala</option>
                            <option value="Tamil">Tamil</option>
                            <option value="English">English</option>

                            </select>

                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="control-label">Highest Educational Qualification</label>

                            <input type="text" name="add_tea_highest_edu_quali" id="add_tea_highest_edu_quali"
                                class="form-control" />

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label class="control-label">Highest Employment Qualification </label>

                            <input type="text" name="add_tea_highest_emp_quali" id="add_tea_highest_emp_quali"
                                class="form-control" />

                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Category of Appointment</label>

                            <select name="add_tea_categ_of_appoint" id="add_tea_categ_of_appoint"
                                class="form-control" />
                            <option value="Permanent">Permanent</option>
                            <option value="Part Time">Part Time</option>
                            <option value="Contract">Contract</option>
                            <option value="Temporary">Temporary</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Subject of Appointment</label>

                            <input type="text" name="add_tea_subj_of_appoint" id="add_tea_subj_of_appoint"
                                class="form-control" />

                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Permenant Address</label>

                            <input type="text" name="add_tea_per_addre" id="add_tea_per_addre" class="form-control" />

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Current Address : </label>

                            <input type="text" name="add_tea_cur_addre" id="add_tea_cur_addre" class="form-control" />

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Telephone </label>

                            <input type="text" name="add_tea_tel_no" id="add_tea_tel_no" class="form-control" />

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Email : </label>

                            <input type="text" name="add_tea_email" id="add_tea_email" class="form-control" />

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Select Profile Photo </label>
                        </div>
                        <input type="file" name="add_tea_photo" id="add_tea_photo" />
                        <span id="store_image"></span>

                    </div>
                </div>

                <input type="hidden" name="hdnid" id="hdnid" />
        </div>
        <div class="card-footer">
            <button type="reset" class="btn btn-primary">Clear</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
        </form>
    </div>
</div>
@endsection
@section('custom-js')
<script type="text/javascript">
$.ajaxSetup({
    beforeSend: function(xhr, type) {
        if (!type.crossDomain) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        }
    },
});




$('#frmaddteacher').submit(function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',

        url: baseurl + '/admin/teacher/',
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        data: new FormData(this),
        success: function (data) {

            if (data.success == true) {
                $.notify("Sucessfully", "success");
                setTimeout(function () {
                    location.reload();
                }, 20);
            } else {
                jQuery.each(data.errors, function (key, value) {
                    $.notify('' + value + '');
                });
            }
        }
    });
});


</script>
<script type="text/javascript">
    $(".datepicker").datepicker({
         autoclose:true,
         todayHighlight:true,
         format:"dd.mm.yyyy",

     });
   </script>


@endsection
