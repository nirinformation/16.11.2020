<!-- Modal -->
<div class="modal fade" id="teachereditmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="alert alert-danger" style="display:none"></div>
      <form method="POST" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
        <div class="modal-body">
          @csrf
          @method('PATCH')

          <div class="row">
            <div class="col-lg-2">
              <div class="form-group">
                <label for="edit_tea_employee_no">Employee No</label>

                <input type="text" name="edit_tea_employee_no" id="edit_employee_no" class="form-control" />

              </div>
            </div>
            <div class="col-lg-4">
              <div class="form-group">
                <label for="edit_tea_name_w_ini">Name with Intitials</label>
                <input type="text" name="edit_tea_name_w_ini" id="edit_tea_name_w_ini" class="form-control" />
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="edit_tea_full_name">Full Name</label>
                <input type="text" name="edit_tea_full_name" id="edit_tea_full_name" class="form-control" />
              </div>
            </div>
          </div>
          <div class="row">

            <div class="col-lg-3">
              <div class="form-group">
                <label name="edit_tea_nic">NIC No</label>
                <input type="text" name="edit_tea_nic" id="edit_tea_nic" class="form-control" />
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="control-label">Date of Birth</label>

                <input type="text" name="edit_tea_dob" id="edit_tea_dob" class="form-control datepicker" />
              </div>

            </div>

            <div class="col-lg-3">
              <div class="form-group">
                <label class="control-label">Gender </label>

                <select name="edit_tea_gender" id="edit_tea_gender" class="form-control" />
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                </select>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="control-label">Civil Status </label>

                <select name="edit_tea_civil_status" id="edit_tea_civil_status" class="form-control" />
                <option value="Married">Married</option>
                <option value="Single">Single</option>
                <option value="Male">Divorced</option>
                <option value="Female">Other</option>
                </select>

              </div>
            </div>
          </div>
          <div class="row">

            <div class="col-lg-4">
              <div class="form-group">
                <label class="control-label">Date of Service Start On</label>

                <input type="text" name="edit_tea_service_start_on" id="edit_tea_service_start_on" class="form-control datepicker" />

              </div>
            </div>
            <div class="col-lg-5">
              <div class="form-group">
                <label class="control-label">Date of Service Start On (This School)</label>

                <input type="text" name="edit_tea_service_start_on_this_school" id="edit_tea_service_start_on_this_school" class="form-control datepicker" />

              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="control-label">Home Town</label>

                <input type="text" name="edit_tea_home_town" id="edit_tea_home_town" class="form-control" />
              </div>
            </div>

          </div>
          <div class="row">
            <div class="col-lg-3">
              <div class="form-group">
                <label class="control-label">Type of Transport</label>

                <input type="text" name="edit_tea_type_of_trans" id="edit_tea_type_of_trans" class="form-control" />

              </div>
            </div>

            <div class="col-lg-3">
              <div class="form-group">
                <label class="control-label">Service Medium</label>

                <select name="edit_tea_service_medium" id="edit_tea_service_mediumstu_per_addre" class="form-control" />
                <option value="Sinhala">Sinhala</option>
                <option value="Tamil">Tamil</option>
                <option value="English">English</option>

                </select>

              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label class="control-label">Highest Educational Qualification</label>

                <input type="text" name="edit_tea_highest_edu_quali" id="edit_tea_highest_edu_quali" class="form-control" />

              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-5">
              <div class="form-group">
                <label class="control-label">Highest Employment Qualification </label>

                <input type="text" name="edit_tea_highest_emp_quali" id="edit_tea_highest_emp_quali" class="form-control" />

              </div>
            </div>

            <div class="col-lg-4">
              <div class="form-group">
                <label class="control-label">Category of Appointment</label>

                <select name="edit_tea_categ_of_appoint" id="edit_tea_categ_of_appoint" class="form-control" />
                <option value="Permanent">Permanent</option>
                <option value="Part Time">Part Time</option>
                <option value="Contract">Contract</option>
                <option value="Temporary">Temporary</option>
                </select>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="control-label">Subject of Appointment</label>

                <input type="text" name="edit_tea_subj_of_appoint" id="edit_tea_subj_of_appoint" class="form-control" />

              </div>
            </div>


          </div>
          <div class="row">
            <div class="col-lg-3">
              <div class="form-group">
                <label class="control-label">Permenant Address</label>

                <input type="text" name="edit_tea_per_addre" id="edit_tea_per_addre" class="form-control" />

              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="control-label">Current Address : </label>

                <input type="text" name="edit_tea_cur_addre" id="edit_tea_cur_addre" class="form-control" />

              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="control-label">Telephone </label>

                <input type="text" name="edit_tea_tel_no" id="edit_tea_tel_no" class="form-control" />

              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label class="control-label">Email : </label>

                <input type="text" name="edit_tea_email" id="edit_tea_email" class="form-control" />

              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-3">
              <div class="form-group">
                <label class="control-label">Select Profile Photo </label>
              </div>
              <input type="file" name="edit_tea_photo" id="edit_tea_photo" />
              <span id="store_image"></span>

            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>