<!-- Modal -->
<div class="modal fade in" id="parentsaddmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Parents</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="alert alert-danger" style="display:none"></div>
      <form id="frmaddparents" method="POST" enctype="multipart/form-data">
        <div class="modal-body">
          {{csrf_field()}}
          <div class="row">
            <div class="col-lg-2">
              <div class="form-group">
                <label for="add_parents_stu_id">Student Id </label>
                <input type="text" name="add_parents_stu_id" id="add_parents_stu_id" value="{{ $std_id }}" class="form-control" readonly />
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label for="add_parents_type">Type Name</label>

                <select name="add_parents_type" id="add_parents_type" class="form-control" />
                <option value="father">Father</option>
                <option value="mother">Mother</option>
                <option value="guardian">Guardian</option>
                </select>

              </div>
            </div>
            <div class="col-lg-8">
              <div class="form-group">
                <label for="add_parents_type">Name </label>
                <input type="text" name="add_parents_name" id="add_parents_name" class="form-control" />
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="add_parents_occu">Occupation</label>

                <input type="text" name="add_parents_occu" id="add_parents_occu" class="form-control" />
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="add_parents_nic">Nic No.</label>

                <input type="text" name="add_parents_nic" id="add_parents_nic" class="form-control" />
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="add_parents_mobile">Mobile No.</label>

                <input type="text" name="add_parents_mobile" id="add_parents_mobile" class="form-control" />
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-3">
              <div class="form-group">
                <label for="add_parents_email">Email Address</label>

                <input type="text" name="add_parents_email" id="add_parents_email" class="form-control" />
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="add_parents_name_of_employ">Name of Employment</label>

                <input type="text" name="add_parents_name_of_employ" id="add_parents_name_of_employ" class="form-control" />
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="add_parents_addre_of_employ">Address of Employement </label>


                <input type="text" name="add_parents_addre_of_employ" id="add_parents_addre_of_employ" class="form-control" />
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="add_parents_office_tel">Office Telephone No.</label>

                <input type="text" name="add_parents_office_tel" id="add_parents_office_tel" class="form-control" />
              </div>
            </div>
          </div>
          <input type="hidden" name="hdnid" id="hdnid" />


          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
      </form>
    </div>
  </div>
</div>