<!-- Modal -->
<div class="modal fade in" id="parentseditmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Parents</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="alert alert-danger" style="display:none"></div>
      <form method="POST" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
        <div class="modal-body">
          @csrf
          @method('PATCH')

          <div class="row">
            <div class="col-lg-2">
              <div class="form-group">
                <label for="edit_parents_stu_id">Student Id </label>
                <input type="text" name="edit_parents_stu_id" id="edit_parents_stu_id" value="{{ $std_id }}" class="form-control" readonly />
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label for="edit_parents_type">Type Name</label>

                <select name="edit_parents_type" id="edit_parents_type" class="form-control" />
                <option value="father">Father</option>
                <option value="mother">Mother</option>
                <option value="guardian">Guardian</option>
                </select>

              </div>
            </div>
            <div class="col-lg-8">
              <div class="form-group">
                <label for="edit_parents_type">Name </label>
                <input type="text" name="edit_parents_name" id="edit_parents_name" class="form-control" />
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="edit_parents_occu">Occupation</label>

                <input type="text" name="edit_parents_occu" id="edit_parents_occu" class="form-control" />
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="edit_parents_nic">Nic No.</label>

                <input type="text" name="edit_parents_nic" id="edit_parents_nic" class="form-control" />
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="edit_parents_mobile">Mobile No.</label>

                <input type="text" name="edit_parents_mobile" id="edit_parents_mobile" class="form-control" />
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-3">
              <div class="form-group">
                <label for="edit_parents_email">Email Address</label>

                <input type="text" name="edit_parents_email" id="edit_parents_email" class="form-control" />
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="edit_parents_name_of_employ">Name of Employment</label>

                <input type="text" name="edit_parents_name_of_employ" id="edit_parents_name_of_employ" class="form-control" />
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="edit_parents_addre_of_employ">Address of Employement </label>


                <input type="text" name="edit_parents_addre_of_employ" id="edit_parents_addre_of_employ" class="form-control" />
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="edit_parents_office_tel">Office Telephone No.</label>

                <input type="text" name="edit_parents_office_tel" id="edit_parents_office_tel" class="form-control" />
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>