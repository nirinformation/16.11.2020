
<table style="border-collapse: collapse;width: 100%;">
    <tr>

        <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">First Name</th>
        <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">{{$data['fname']}}</td>
    </tr>
    <tr>
        <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Last Name</th>
        <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">{{$data['lname']}}</td>
    </tr>
    <tr>
        <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Mobile</th>
        <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">{{$data['mobile']}}</td>
    </tr>
    <tr>
        <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Telephone</th>
        <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">{{$data['telephone']}}</td>
    </tr>
    <tr>
        <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Date of Birth</th>
        <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">{{$data['dob']}}</td>
    </tr>
    <tr>
        <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Email</th>
        <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">{{$data['email']}}</td>
    </tr>
    <tr>
        <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">NIC</th>
        <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">{{$data['nic']}}</td>
    </tr>
</table>

