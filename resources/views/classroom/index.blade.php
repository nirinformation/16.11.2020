@extends('dashboard.index')
@section('content')
    <div class="row">

        <div class="col-md-12 text-right">
           <button class="btn btn-primary" onclick="loadAddModal()">Add Class Room</button>
        </div>
        <div class="col-md-12 mt-3">
            <table class="table table-bordered table-hover" id="tblclassroom">
                <thead>
                    <tr>
                        <th>Section</th>
                        <th>Grade</th>
			            <th>Class Room</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th>Section</th>
                        <th>Grade</th>
			            <th>Class Room</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@include('classroom.components.edit-classroom-modal')
@include('classroom.components.add-classroom-modal')
@endsection

@section('custom-js')
<script type="text/javascript">
$.ajaxSetup({
    beforeSend: function(xhr, type) {
        if (!type.crossDomain) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        }
    },
});
   
    $(document).ready( function () {
    $('#tblclassroom').DataTable({
    ajax: baseurl+'/get-classroom-data',
    columns: [
        { data: 'sec_name' },
        { data: 'gra_name' },
        { data: 'clas_room_name' },
        {
            data: null,
            className: "center",
            defaultContent: '<a href="" class="editor_edit btn btn-primary inner"><i class="fas fa-edit"></i></a>' +
                    '<a href="" class="remove_user btn btn-danger inner"><i class="fas fa-trash-alt"></i></a>'
        }

    ]
});

});

// Edit record
$('#tblclassroom').on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        var data = $('#tblclassroom').DataTable().row($(this).parents('tr')).data();
        $('#edit_clas_room_sec_id').val(data.sec_name);
        $('#edit_clas_room_gra_id').val(data.gra_name);
        $('#edit_clas_room_name').val(data.clas_room_name);
        $('#hdnid').val(data.clas_room_id);
        $('#classroomeditmodal').modal('toggle');

});

$('#frmeditclassroom').submit(function(e){
    e.preventDefault();
    var id = $('#hdnid').val();

    $.ajax({
        type:'PUT',
        url: baseurl+'/admin/classroom/'+id,
        data: $('#frmeditclassroom').serialize(),
        success: function(data){
            if(data.success==true){
                alert(data.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<p>'+value+'</p>');
                          });
            }
        }
    });

});
$('#frmaddclassroom').submit(function(e){
    e.preventDefault();
    $.ajax({
        type:'POST',

        url: baseurl+'/admin/classroom/',
        contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
        data: new FormData(this),
        success: function(data){

            if(data.success==true){
                alert(data.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
               
                jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<p>'+value+'</p>');
                          });
                                  
                        
            }
           
        }
    });

});
//Add rocord
function loadAddModal(){
    $('#classroomaddmodal').modal('toggle');
    
}

$('#tblclassroom').on('click', 'a.remove_user', function (e) {
        e.preventDefault();
       
        var data = $('#tblclassroom').DataTable().row($(this).parents('tr')).data();
        var id=data.clas_room_id;
        
        $.confirm({
    title: 'Confirm!',
    content: 'Simple confirm!',
    buttons: {
        confirm: function () {
            $.ajax({
        type:'delete',
        url: baseurl+'/admin/classroom/'+id,
        
        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg);
            }
        }
    });
        },
        cancel: function () {
            
        },
        
    }
});








        /* $.ajax({
        type:'DELETE',
        url: baseurl+'/admin/classroom/'+id,
        
        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg);
            }
        }
    }); */


});


</script>
<script>
  $(document).ready(function(){

 $('#add_clas_room_sec_id').on('click',function(e){
   console.log(e);
 var sect_id=e.target.value;
 $.get('/gradedynamic?sect_id='+sect_id,function(data){
 
  $('#add_clas_room_gra_id').empty();
  $('#add_clas_room_gra_id').append('<option value="0" disable="true" selected="true">==Select Grades== </option>');
  
  $.each(data, function(index,allclassObj){
    $('#add_clas_room_gra_id').append('<option value=" '+allclassObj.gra_id+'">'+allclassObj.gra_name+'</option>');
  });

 });
});
 
  });
  </script>
<script>
    $(document).ready(function(){
  
   $('#edit_clas_room_sec_id').on('click',function(e){
     console.log(e);
   var sect_id=e.target.value;
   $.get('/gradedynamic?sect_id='+sect_id,function(data){
   
    $('#edit_clas_room_gra_id').empty();
    $('#edit_clas_room_gra_id').append('<option value="0" disable="true" selected="true">==Select Grades== </option>');
    
    $.each(data, function(index,allclassObj){
      $('#edit_clas_room_gra_id').append('<option value=" '+allclassObj.gra_id+'">'+allclassObj.gra_name+'</option>');
    });
  
   });
  });
   
    });
    </script>
@endsection
