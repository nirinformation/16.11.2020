<?php


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudentSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->insert([
            'fname' => 'Admin',
            'lname' => 'User',
            'mobile' => '0785278512',
            'telephone' => '0422262174',
            'email' => 'kamal@gmail.com',
            'dob' => '2020-12-20',
            'nic' => '0770152525',
            'status' => 1,
            'created_by' => 0,
            'updated_by' => 0,
        ]);
    }
}