<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class CourseBatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('course_batch')->insert([
            'course_id'=>1,
            'batch_code'=>'JAVA-001',
            'lecturer_id'=>1,
            'course_fee'=>20000,
            'start_date'=>'2020-07-10',
            'end_date'=>'2020-12-20',
            'created_by'=>0,
            'updated_by'=>0,
        ]);
        DB::table('course_batch')->insert([
            'course_id'=>1,
            'batch_code'=>'JAVA-002',
            'lecturer_id'=>1,
            'course_fee'=>25000,
            'start_date'=>'2020-10-10',
            'end_date'=>'2021-04-20',
            'created_by'=>0,
            'updated_by'=>0,
        ]);
        DB::table('course_batch')->insert([
            'course_id'=>2,
            'batch_code'=>'ANG-001',
            'lecturer_id'=>1,
            'course_fee'=>30000,
            'start_date'=>'2020-8-10',
            'end_date'=>'2020-12-20',
            'created_by'=>0,
            'updated_by'=>0,
        ]);
    }
}
