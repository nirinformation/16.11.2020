<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblTeacherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_teacher', function (Blueprint $table) {
            $table->increments('tea_id');
            $table->integer('tea_employee_no')->unique();
            $table->string('tea_name_w_ini')->nullable();
            $table->string('tea_full_name')->nullable();
            $table->string('tea_nic')->nullable();
            $table->date('tea_dob')->nullable();
            $table->string('tea_gender')->nullable();
            $table->string('tea_civil_status')->nullable();
            $table->date('tea_service_start_on')->nullable();
            $table->date('tea_service_start_on_this_school')->nullable();
            $table->string('tea_home_town')->nullable();
            $table->string('tea_type_of_trans')->nullable();
            $table->string('tea_service_medium')->nullable();
            $table->string('tea_highest_edu_quali')->nullable();
            $table->string('tea_highest_emp_quali')->nullable();
            $table->string('tea_categ_of_appoint')->nullable();
            $table->string('tea_subj_of_appoint')->nullable();
            $table->string('tea_per_addre')->nullable();
            $table->string('tea_cur_addre')->nullable();
            $table->string('tea_tel_no')->nullable();
            $table->string('tea_email')->nullable();
            $table->string('tea_photo')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_teacher');
    }
}