<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblExamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_exam', function (Blueprint $table) {
            $table->increments('exam_id');
            $table->string('exam_name')->unique();
            $table->string('exam_category');
            $table->string('exam_aca_year');
            $table->string('exam_term');
	        $table->date('exam_start_date');
            $table->date('exam_end_date');
            $table->string('exam_status');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_exam');
    }
}