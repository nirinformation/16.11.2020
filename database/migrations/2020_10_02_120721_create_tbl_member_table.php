<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_member', function (Blueprint $table) {
           $table->increments('member_id');
            $table->integer('member_employee_no')->unique();
            $table->string('member_name_w_ini')->nullable();
            $table->string('member_full_name')->nullable();
            $table->string('member_nic')->nullable();
            $table->date('member_dob')->nullable();
            $table->string('member_gender')->nullable();
            $table->string('member_civil_status')->nullable();
            $table->date('member_service_start_on')->nullable();
            $table->date('member_service_start_on_this_school')->nullable();
            $table->string('member_home_town')->nullable();
            $table->string('member_type_of_trans')->nullable();
            $table->string('member_service_medium')->nullable();
            $table->string('member_highest_edu_quali')->nullable();
            $table->string('member_highest_emp_quali')->nullable();
            $table->string('member_categ_of_appoint')->nullable();
            $table->string('member_subj_of_appoint')->nullable();
            $table->string('member_per_addre')->nullable();
            $table->string('member_cur_addre')->nullable();
            $table->string('member_tel_no')->nullable();
            $table->string('member_email')->nullable();
            $table->string('member_photo')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_member');
    }
}
